<?php
/**
 * Created by PhpStorm.
 * User: jarno
 * Date: 23.10.15
 * Time: 10.16
 */

add_action('customize_register', function ( $wp_customize ) {
    $wp_customize->add_section('nokiankylat_some', array(
        'title' => 'Sosiaalinen media'
    ));

    $wp_customize->add_setting( 'nokiankylat_facebook', array(
        'sanitize_callback' => 'esc_url',
        'type' => 'option',
    ));
    $wp_customize->add_control( 'nokiankylat_facebook', array(
        'label' => 'Facebook',
        'section' => 'nokiankylat_some',
        'type' => 'url',
        'description' => 'Facebook-sivun tai -ryhmän verkko-osoite',
        'input_attrs' => [ 'placeholder' => 'http://facebook.com/...']
    ));
});