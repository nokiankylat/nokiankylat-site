<?php

/**
 * Customizations for The Events Calendar plugin.
 *
 * @link https://wordpress.org/plugins/the-events-calendar/
 * @link https://theeventscalendar.com/knowledgebase/themers-guide/
 */
namespace Wysiwyg\NokianKylat\TheEventsCalendar;

/**
 * Displays a calendar sheet before the event description.
 */
function calendar_sheet() { ?>
	<div class="sheet">
		<span class="month"><?= tribe_get_start_date(null, false, 'M'); ?></span>
 		<span class="days">
			<span class="day"><?= tribe_get_start_date(null, false, 'j'); ?></span>
			<span class="weekday"><?= tribe_get_start_date(null, false, 'D'); ?></span>
		</span>
	</div><!--  /.sheet -->
	<?php
}
add_action('tribe_events_list_widget_before_the_event_title', __NAMESPACE__ . '\\calendar_sheet');


function event_schedule_settings($settings) {
	$settings['time'] = false;
	return $settings;
}
add_filter('tribe_events_event_schedule_details_formatting', __NAMESPACE__ . '\\event_schedule_settings');

function event_schedule_link($inner) {
	return '<a href="' . esc_url( tribe_get_event_link() ) . '" rel="bookmark">' . $inner . '</a>';
}
add_filter('tribe_events_event_schedule_details_inner', __NAMESPACE__ . '\\event_schedule_link');