<?php

/**
 * Customizations for Display Posts Shortcode plugin.
 *
 * @link http://wordpress.org/extend/plugins/display-posts-shortcode/
 * @link https://github.com/billerickson/display-posts-shortcode/
 * @link http://www.billerickson.net/code-tag/display-posts-shortcode/
 */
namespace Wysiwyg\NokianKylat\DisplayPostsShortcode;

/**
 * Set defaults in Display Posts Shortcode
 *
 * @param array $out, the output array of shortcode attributes (after user-defined and defaults have been combined)
 * @param array $pairs, the supported attributes and their defaults
 * @param array $atts, the user defined shortcode attributes
 * @return array $out, modified output
 */
function defaults( $out, $pairs, $atts ) {
  $new_defaults = array( 
    'posts_per_page' => 2,
    'include_date' => true,
    'include_excerpt' => true,
    'image_size' => 'thumbnail',
    'date_format' => 'j.n.Y'
  );
  
  foreach( $new_defaults as $name => $default ) {
    if( array_key_exists( $name, $atts ) )
      $out[$name] = $atts[$name];
    else
      $out[$name] = $default;
  }
  
  return $out;
}
add_filter( 'shortcode_atts_display-posts', __NAMESPACE__ . '\\defaults', 10, 3 );

/**
 * Customize the output of the shortcode.
 *
 * @param string $output the original markup for an individual post
 * @param array $atts all the attributes passed to the shortcode
 * @param string $image the image part of the output
 * @param string $title the title part of the output
 * @param string $date the date part of the output
 * @param string $excerpt the excerpt part of the output
 * @param string $inner_wrapper what html element to wrap each post in (default is li)
 * @return string $output the modified markup for an individual post
 */
function output( $output, $atts, $image, $title, $date, $excerpt, $inner_wrapper ) {
  
  $classes = 'listing-item row';

  $readmore = $excerpt ? 
    '<a 
      role="button" 
      class="button info small radius" 
      href="' . get_permalink() . '"
    >Lue lisää</a>' :
    '';

  $textClasses = $image ? "small-8 columns" : "small-12 columns last";

  $imageHtml = $image ? "
      <div class=\"small-4 columns last\">
        $image
      </div>" 
      : "";

  $output = " 
    <$inner_wrapper class=\"$classes\">
      <div class=\"$textClasses\">
        <h3>$title</h3>
        $date
        $excerpt
        $readmore
      </div>
      $imageHtml
    </$inner_wrapper>
  ";

  return $output;
}
add_filter( 'display_posts_shortcode_output', __NAMESPACE__ . '\\output', 10, 7 );

