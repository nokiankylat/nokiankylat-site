<?php
/**
 * Customizations related to Events Manager plugin.
 */
namespace Wysiwyg\NokianKylat\Plugin\EventsManager;

const SIDEBAR_EVENT_LIST_HEADER_FORMAT = '<ul class="sidebar-event-list">';
const SIDEBAR_EVENT_LIST_FOOTER_FORMAT = '</ul><!--  /.sidebar-event-list -->';
const SIDEBAR_EVENT_LIST_ITEM_FORMAT = '
    <li class="event">
        <div class="sheet">
            <span class="month">#M</span>
            <span class="days">
                <span class="day">#j</span>
                <span class="weekday">#D</span>
            </span>
        </div><!--  /.sheet -->
        <div class="event-description">
            <a href="#_EVENTURL">
                <span class="event-name">#_EVENTNAME</span>
                <span class="event-date">#_EVENTDATES</span>
            </a>
        </div><!--  /.entry-description -->
    </li><!--  /.event -->
';

function event_dates( $event ) {
	$start_year = date( 'Y', $event->start );
	$end_year = date( 'Y', $event->end );

	$start_month = date( 'n', $event->start );
	$end_month = date( 'n', $event->end );

	$start_day = date( 'j', $event->start );
	$end_day = date( 'j', $event->end );

	if ( $start_year != $end_year ) {
		return date_i18n( 'j.n.Y', $event->start ) . '&ndash;' . date_i18n( 'j.n.Y', $event->end );
	} else if ( $start_month != $end_month ) {
		return date_i18n( 'j.n.', $event->start ) . '&ndash;' . date_i18n( 'j.n.Y', $event->end );
	} else if ( $start_day != $end_day ) {
		return date_i18n( 'j.', $event->start ) . '&ndash;' . date_i18n( 'j.n.Y', $event->end );
	} else {
		return date_i18n( 'j.n.Y', $event->start );
	}
}

add_filter('em_event_output_placeholder', function ( $output, $event, $placeholder ) {
    if ( $placeholder == '#_EVENTDATES' ) {
        $output = event_dates( $event );
    }
    return $output;
}, 1, 3 );


/**
 * Widget that shows a list of events from Events Manager.
 */
class Event_List_Widget extends \WP_Widget {
    function __construct( ) {
        parent::__construct('nokiankylat_event_list', $name = 'Tapahtumaluettelo');	
    }

    function widget( $args, $instance ) {
		$events = $this->get_events();
		if ( count( $events ) > 0 ) {
	    	echo $args['before_widget'];
		    echo $args['before_title'] . 'Tulevia tapahtumia' . $args['after_title'];
		    $this->output_events( $events );
			$this->output_all_events_link();		
		    echo $args['after_widget'];
		} else {
		    echo '<!-- No events -->';
		}
    }

    function output_events( $events ) {
        echo \EM_Events::output( $events, [
            'scope' => 'future',
            'pagination' => 0,
            'ajax' => 0,
            'format_header' => SIDEBAR_EVENT_LIST_HEADER_FORMAT,
            'format_footer' => SIDEBAR_EVENT_LIST_FOOTER_FORMAT,
            'format' => SIDEBAR_EVENT_LIST_ITEM_FORMAT,
        ]);
	}

	function output_all_events_link() { ?>
		<div class="all-events">
			<?= em_get_link( 'Kaikki tapahtumat' ); ?>
		</div><!--  /.all-events --><?php
	}

	function get_events($count = 5) {
		$result = array();
		$encountered_recurrences = array();

		foreach ( \EM_Events::get() as $event ) {
			if ( $count == 0 ) {
				break;
			}

			// Only return the first occurrence of recurrent events
			if ( $event->recurrence_id ) {
				if ( in_array( $event->recurrence_id, $encountered_recurrences ) ) {
					// Already found an occurrence of this recurrence, don't return this
					continue;
				}

				$encountered_recurrences[] = $event->recurrence_id;
			}

			$result[] = $event;
			$count--;
		}

		return $result;
	}
}

add_action( 'widgets_init', function() {
	register_widget( __NAMESPACE__ . '\Event_List_Widget' );
});