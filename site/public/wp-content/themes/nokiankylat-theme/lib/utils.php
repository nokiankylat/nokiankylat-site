<?php

namespace Roots\Sage\Utils;

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', __NAMESPACE__ . '\\get_search_form');

function sidebar_inherited( $acf_field, $sidebar ) {
    global $post;

    if ( get_field( $acf_field) ) {
        the_field( $acf_field );
        return;
    } else if ( is_single() ) {
        $ancestors = get_post_ancestors( $post );
        foreach ( $ancestors as $post_id ) {
            if ( get_field( $acf_field, $post_id ) ) {
                the_field( $acf_field, $post_id );
                return;
            }
        }
    }

    dynamic_sidebar( $sidebar );
}
