<?php

namespace Roots\Sage\Config;

use Roots\Sage\ConditionalTagCheck;

/**
 * Enable theme features
 */
add_theme_support('soil-clean-up');         // Enable clean up from Soil
add_theme_support('soil-nav-walker');       // Enable cleaner nav walker from Soil
add_theme_support('soil-relative-urls');    // Enable relative URLs from Soil
add_theme_support('soil-nice-search');      // Enable nice search from Soil
add_theme_support('soil-jquery-cdn');       // Enable to load jQuery from the Google CDN

/**
 * Configuration values
 */
if (!defined('WP_ENV')) {
  // Fallback if WP_ENV isn't defined in your WordPress config
  // Used in lib/assets.php to check for 'development' or 'production'
	define('WP_ENV', 'production');
}

if (!defined('DIST_DIR')) {
  // Path to the build directory for front-end assets
	define('DIST_DIR', '/dist/');
}

/**
 * Define which pages shouldn't have the right sidebar
 */
function display_sidebar() {
	static $display;

	if (!isset($display)) {
		if (is_404() || get_field('right_sidebar_displayed') == false) {
			$display = false;
		} else {
			$display = true;
		}
		$display = apply_filters('sage/display_sidebar', $display);
	}

	return $display;
}

/**
 * Define which pages shouldn't have the left sidebar
 */
function display_left_sidebar() {
	static $display;

	if (!isset($display)) {
		if (is_404() || get_field('left_sidebar_displayed') == false) {
			$display = false;
		} else {
			$display = true;
		}
		$display = apply_filters('sage/display_left_sidebar', $display);
	}

	return $display;
}
