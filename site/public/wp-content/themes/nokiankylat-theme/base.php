<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;
use Wysiwyg\NokianKylat\Plugin;

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action( 'get_header' );
      get_template_part( 'templates/header' );
    ?>
    <div class="wrap container" role="document">
      <div class="content">
        
        <?php if ( get_field( 'featured_posts' ) ): ?>
          <div class="page-banner row medium-collapse">
            <div class="small-12 columns">
                <?php get_template_part( 'templates/featured-posts' ); ?>
            </div><!--  /.small-12 columns -->
          </div><!--  /.page-banner row -->
        <?php elseif ( has_post_thumbnail() ): ?>
          <div class="page-banner row collapse">
              <div class="small-12 columns">
                <div class="post-thumbnail">
                  <?php the_post_thumbnail(); ?>
                  <?php Plugin\the_post_thumbnail_caption(); ?>
                </div><!--  /.post-thumbnail -->
              </div><!--  /.small-12 columns -->
          </div><!--  /.page-banner row -->
        <?php endif; ?>
        
        <div class="row">
          
          <main 
            class="main small-12<?php 
              if (Config\display_left_sidebar()) :
                if (Config\display_sidebar()) :
                  ?> medium-6<?php 
                else: 
                  ?> medium-9<?php 
                endif; ?> medium-push-3<?php
              endif; ?> columns"
            role="main"
          >
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->

          <?php if (Config\display_left_sidebar()) : ?>
            <aside 
              class="sidebar left-sidebar small-12 medium-3<?php 
                if (Config\display_sidebar()): ?> 
                  medium-pull-6<?php 
                else: ?>
                  medium-pull-9<?php
                endif; ?> columns" 
              role="complementary">
                <?php get_template_part( 'templates/left-sidebar', get_post_type() ); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
          
          <?php if (Config\display_sidebar()) : ?>
            <aside class="sidebar right-sidebar small-12 medium-3 columns" role="complementary">
                <?php get_template_part( 'templates/right-sidebar', get_post_type() ); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>

        </div><!-- /.row -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
