<?php $boxes = get_field('featured_posts'); ?>
<?php if ($boxes): ?>
	<ul class="featured-posts small-block-grid-1 medium-block-grid-3">
		<?php foreach ($boxes as $i => $box): ?>
			<li class="featured-post<?php if ($i > 0) { echo ' show-for-medium-up'; } ?>">
				<div class="image">
					<?php
						$imageId = $box['image'] ? $box['image'] :
						           ($box['target'] ? get_post_thumbnail_id($box['target']->ID) : null);
						if ($imageId) {
							$image = wp_get_attachment_image($imageId, 'featured-post');
						}

						$url = $box['target_url'] ? esc_attr($box['target_url']) :
						       ($box['target'] ? get_permalink($box['target']->ID) : null);
					?>
					<?php if ($image): ?>
						<?php if ($url): ?><a href="<?php echo $url; ?>"><?php endif; ?>
							<?php echo $image; ?>
						<?php if ($url): ?></a><?php endif; ?>
					<?php endif; ?>
				</div><!--  /.image -->
				<div class="description">
					<?php if ($url): ?><a href="<?php echo $url; ?>"><?php endif; ?>
						<?php echo esc_html($box['description']); ?>
					<?php if ($url): ?><span class="more-link"><?= __( 'Lue lisää' );?></span></a><?php endif; ?>
				</div><!--  /.description -->
			</li><!--  /.featured-post -->
		<?php endforeach; ?>
	</ul><!--  /.featured-posts -->
<?php endif; // featured-posts ?>
