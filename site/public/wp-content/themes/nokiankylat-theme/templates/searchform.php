<form role="search" method="get" class="search" action="<?= esc_url(home_url('/')); ?>">
    <input
        type="text"
        value="<?php echo get_search_query(); ?>"
        name="s"
        placeholder="Haku"
    >
    <i class="fa fa-search"></i>
</form>