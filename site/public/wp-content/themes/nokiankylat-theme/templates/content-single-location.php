<?php use Wysiwyg\NokianKylat\Plugin\EventsManager; ?>

<?php while (have_posts()) : the_post(); ?>
	<?php $event = new EM_Event($post); ?>
	<article <?php post_class(); ?>>
		<header>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header>
		<div class="entry-content">
            <?php
                if ( get_option('dbem_cp_locations_formats') ) {
                    the_content();
                } else {
                    global $EM_Location;
                    echo $EM_Location->output('
                        <ul class="tapahtuma-aika-ja-paikka">
                            <li><i class="fa fa-map-marker fa-fw"></i> #_LOCATIONADDRESS, #_LOCATIONPOSTCODE #_LOCATIONTOWN</li>
                        </ul>
                        <div class="locationnotes">
                            #_LOCATIONNOTES
                        </div>


                    ');
                }
            ?>
		</div>
		<footer>
			<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
		</footer>
		<?php comments_template('/templates/comments.php'); ?>
	</article>
<?php endwhile; ?>
