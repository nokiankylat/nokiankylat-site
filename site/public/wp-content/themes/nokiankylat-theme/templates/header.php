<header>
    <div class="banner-wrapper contain-to-grid">
        <section class="top-bar" data-topbar role="banner">
            <ul class="title-area">
                <li class="name"><h1><a href="<?php echo esc_url(home_url()); ?>"><?php bloginfo('name'); ?></a></h1>
                </li>
            </ul>
            <section class="top-bar-section">
                <ul class="right">
                    <?php if ( get_option( 'nokiankylat_facebook' ) ): ?>
                        <li class="some">
                            <a href="<?php echo get_option( 'nokiankylat_facebook' ) ?>">
                                <i class="fa fa-facebook-official fa"></i>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="search has-form">
                        <?php get_search_form(); ?>
                    </li>
                </ul>
            </section>
        </section>
    </div>

    <div class="nav-wrapper contain-to-grid">
        <nav class="top-bar" data-topbar role="navigation" data-options="custom_back_text:true;back_text:Takaisin">
            <ul class="title-area">
                <li class="name"></li>
                <li class="toggle-topbar menu-icon-left"><a href="#"><span>Sisällysluettelo</span></a></li>
            </ul>
            <section class="top-bar-section">
                <?php if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu([
                        'theme_location' => 'primary_navigation',
                        'menu_class' => 'nav',
                        'walker' => new Wysiwyg\NokianKylat\Menus\Foundation_Walker(['in_top_bar' => true])
                    ]);
                endif; ?>
            </section>
        </nav>
    </div>
</header>
