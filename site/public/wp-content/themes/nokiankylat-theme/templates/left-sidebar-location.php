<?php use Wysiwyg\NokianKylat\Plugin\EventsManager; ?>
<?php global $EM_Location; ?>

<?php echo $EM_Location->output('#_LOCATIONMAP'); ?>

<?php $events_count = EM_Events::count( array('location'=>$EM_Location->location_id, 'scope'=>'future') ); ?>
<?php if ( $events_count > 0 ): ?>
    <div class="tulevia-tapahtumia">
        <h3>Tulevia tapahtumia</h3>
        <?php echo EM_Events::output([
            'location' => $EM_Location->location_id,
            'scope' => 'future',
            'pagination' => 0,
            'ajax' => 0,
            'format_header' => EventsManager\SIDEBAR_EVENT_LIST_HEADER_FORMAT,
            'format_footer' => EventsManager\SIDEBAR_EVENT_LIST_FOOTER_FORMAT,
            'format' => EventsManager\SIDEBAR_EVENT_LIST_ITEM_FORMAT,
            'limit' => 5
        ]); ?>
    </div>
<?php else: ?>
    <span class="ei-tapahtumia">Ei tapahtumia</span>
<?php endif; ?>

