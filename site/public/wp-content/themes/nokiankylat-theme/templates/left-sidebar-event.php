<?php global $EM_Event; ?>
<?php echo $EM_Event->output('#_LOCATIONMAP'); ?>

<ul class="lisaa-kalenteriin">
    <li>
        <i class="fa fa-calendar-plus-o"></i>
        <a href="<?php echo $EM_Event->output('#_EVENTGCALURL'); ?>">Lisää Google Kalenteriin</a>
    </li>
    <li>
        <i class="fa fa-calendar-plus-o"></i>
        <a href="<?php echo $EM_Event->output('#_EVENTICALURL'); ?>">Lisää muuhun kalenteriin</a>
    </li>
</ul>
