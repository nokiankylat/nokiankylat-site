<div class="julkaistu">
    <time class="updated" datetime="<?= get_the_time('c'); ?>"><?= get_the_date(); ?></time> <?= get_the_author(); ?>
</div>