<?php use Wysiwyg\NokianKylat\Plugin\EventsManager; ?>

<?php while (have_posts()) : the_post(); ?>
	<?php $event = new EM_Event($post); ?>
	<article <?php post_class(); ?>>
		<header>
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header>
		<div class="entry-content">
            <?php
                if ( get_option('dbem_cp_events_formats') ) {
                    the_content();
                } else {
                    global $EM_Event;
                    echo $EM_Event->output('
                        <ul class="tapahtuma-aika-ja-paikka">
                            <li><i class="fa fa-clock-o fa-fw"></i> #_EVENTDATES #_EVENTTIMES</li>
                            {has_location}
                                <li><i class="fa fa-map-marker fa-fw"></i> #_LOCATIONLINK, #_LOCATIONADDRESS</li>
                            {/has_location}
                        </ul>
                        #_EVENTNOTES
                        {has_bookings}
                        <h3>Varaukset</h3>
                        #_BOOKINGFORM
                        {/has_bookings}
                    ');
                }
            ?>
		</div>
		<footer>
			<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
		</footer>
		<?php comments_template('/templates/comments.php'); ?>
	</article>
<?php endwhile; ?>
