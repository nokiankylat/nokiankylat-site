��    B      ,  Y   <      �  
   �     �     �     �  	   �  1   �          &     /  
   5     @     E     R     `     m     s     {  
   �  6   �     �     �     �  
   �  	   �                /  	   ;     E     L     Z     n     s     y     �     �  1   �     �     �                     $     *     1     7     Q     a     ~     �     �     �     �     �     �     	     %	     <	  
   T	     _	  	   f	     p	     �	  3   �	     �	  �  �	     S     `     |  	   �     �  :   �  
   �     �     �     �               )     =     M  	   Y     c     }  5   �     �     �     �     �     �  "        +     ?  	   P     Z     `     o     �     �     �     �     �  9   �     $     :     =     Q     X  
   k  
   v     �     �     �     �     �     �  
             $  !   A  !   c     �     �     �     �     �  	   �            3   $     X                 =       1      A   .   >                      ,   /         *      $   (       	          9   B   '                 :       ;         2   5                7       @       ?      %      "   8      -   0   +      #                      
             3              6   !           )   4             <   &           % Comments &laquo; Older Comments &laquo; Previous Image (Edit) 1 Comment <span class="meta-nav">&laquo;</span> Older posts About %s Archives Audio Author: %s Back Button Color Button Colors Cancel Reply Chats Comment Comments are closed. Contact Us Continue reading <span class="meta-nav">&raquo;</span> Daily Archives: %s Default Edit Edit Image Edit Page First Footer Widget Area Footer Content Footer Text Galleries Images Leave a Reply Leave a Reply to %s Left Links Main Menu Position Monthly Archives: %s Newer Comments &raquo; Newer posts <span class="meta-nav">&raquo;</span> Next Image &raquo; No Normal Position Pages: Post Comment Posts Quotes Right Second Footer Widget Area Select position Set Width (Default is 64rem) Set background color Set width (Default is 64rem) Sidebar Skip to content Text &amp; Nav Centered Text Left - Nav Right Text Right - Nav Left Third Footer Widget Area Top of Browser - Fixed Top of Browser - Scroll Try Again? Videos WordPress Yearly Archives: %s Yes http://themeawesome.com/responsive-wordpress-theme/ http://wordpress.org/ Project-Id-Version: WP-Forge 5.5.2
POT-Creation-Date: 2015-05-24 22:07+0200
PO-Revision-Date: 2015-05-24 23:02+0200
Last-Translator: Thomas E. Vasquez <tsquez@gmail.com>
Language-Team: Wysiwyg Oy <info@wysiwyg.fi>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8
Language: fi
 % kommenttia &laquo; Vanhemmat kommentit &laquo; Edellinen kuva (Muokkaa) 1 kommentti <span class="meta-nav">&laquo;</span> Vanhemmat artikkelit Tietoja %s Arkistot Ääni Kirjoittaja: %s Takaisin Painikkeen väri Painikkeiden värit Peruuta vastaus Keskustelut Kommentti Kommentointi on suljettu. Ota yhteyttä Jatka lukemista <span class="meta-nav">&raquo;</span> Päivittäiset arkistot: %s Oletus Muokkaa Muokkaa kuvaa Muokkaa sivua Ensimmäinen alapalkin vimpainalue Alapalkin sisältö Alapalkin teksti Galleriat Kuvat Jätä vastaus Jätä kommentti artikkeliin %s Vasen Linkit Päävalikon sijainti Kuukausittaiset arkistot: %s Uudemmat kommentit &raquo; Uudemmat artikkelit <span class="meta-nav">&raquo;</span> Seuraava kuva &raquo; Ei Tavallinen sijainti Sivut: Lähetä kommentti Artikkelit Lainaukset Oikea Toinen alapalkin vimpainalue Valitse sijainti Aseta leveys (Oletus on 64rem) Aseta taustaväri Aseta leveys (Oletus on 64rem) Sivupalkki Siirry sisältöön Teksti &amp; Navi keskitetty Teksti vasemmalla - Navi oikealla Teksti oikealla - Navi vasemmalla Kolmas alapalkin vimpainalue Selaimen yläosassa - kiinteä Selaimen yläosassa - liikkuva Yritä uudelleen? Videot WordPress Vuosittaiset arkistot: %s Kyllä http://themeawesome.com/responsive-wordpress-theme/ http://wordpress.org/ 