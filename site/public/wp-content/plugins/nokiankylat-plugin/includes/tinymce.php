<?php

namespace Wysiwyg\NokianKylat\Plugin\TinyMCE;

/**
 * Modify TinyMCE 
 *
 * @param array $in
 * @return array $in
 */
add_filter( 'tiny_mce_before_init', function ( $in ) {
    // Keep the "kitchen sink" open:   
    $in[ 'wordpress_adv_hidden' ] = FALSE;

    return $in;
});

/**
 * Customize the first row of buttons.
 */
add_filter('mce_buttons', function ( $buttons ) {
    return [ 
    	'bold', 'italic', 'underline', 'forecolor',
    	'bullist', 'numlist', 'blockquote', 'link', 'unlink', 
    	'outdent','indent', 'alignleft', 'aligncenter', 'alignright',
    	'undo', 'redo', 'wp_help'
    ];
});

/**
 * Customize the second row of buttons.
 */
add_filter('mce_buttons_2', function ( $buttons ) {
	return [
		'formatselect', 'pastetext', 'removeformat', 'charmap'
	];
});
