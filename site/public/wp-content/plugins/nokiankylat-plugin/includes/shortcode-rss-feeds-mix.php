<?php

add_shortcode( 'rss-feeds-mix', function( $atts ) {
	$feed_uris = array(
		'Pinsiö' => 'http://www.pinsioseura.fi/feed/',
		'Sarkola' => 'http://www.sarkola.info/index.php?format=feed&type=rss',
		'Siuro' => 'http://www.siuro.info/?format=feed&type=rss',
		'Sorva' => 'http://www.nokiankylat.fi/sorva/category/uutiset/feed',
		'Taivalkunta' => 'http://www.nokiankylat.fi/taivalkunta/category/uutiset/feed',
		'Tottijärvi' => 'http://www.nokiankylat.fi/tottijarvi/category/uutiset/feed'
	);

	// Retrieve the feed objects:
	$feeds = array_map('fetch_feed', $feed_uris );

	// Store village names for each feed permalink for later use:
	$title_by_feed_permalink = array();
	foreach ($feeds as $title => $feed) {
		$title_by_feed_permalink[$feed->get_permalink()] = $title;
	}

	// Convert feed objects into feed item arrays and replace string keys with numbers:
	$feed_items = array_map(
		function ( $feed ) {
			return $feed->get_items( 0, 3 );
		},
		array_values( $feeds )
	);

	// Combine feed item arrays into one array:
	$combined_feeds = call_user_func_array('array_merge', $feed_items);

	// Filter out items older than one year:
	$combined_feeds = array_filter( $combined_feeds, function( $item ) {
		return $item->get_date('U') > time() - 60 * 60 * 24 * 365;
	});

	// Sort by date:
	usort( $combined_feeds, function( $item1, $item2 ) {
		return $item2->get_date( 'U' ) - $item1->get_date( 'U' );
	});

	return
		'<div class="widget">' .
			'<h2>Uutisia kyliltä</h2>' .
			'<ul>' .
				array_reduce(
					$combined_feeds,
					function ( $carry, $item ) use ( $title_by_feed_permalink ) {
						return
							$carry .
							"<li>\n" .
								'<a href="' . $item->get_permalink() . '">' . $item->get_title() . "</a>\n" .
								'<div class="news-meta">' .
									'<span>' . $title_by_feed_permalink[$item->get_feed()->get_permalink()] . "</span>\n" .
									'<span>' . $item->get_date( 'j.n.Y' ) . "</span>\n" .
								'</div>' .
							"</li>\n";
					},
					''
				) .
			'</ul>' .
		'</div>';
} );
