<?php

add_action('init', function() {
    register_taxonomy_for_object_type('category', EM_POST_TYPE_EVENT);
    register_taxonomy_for_object_type('category', EM_POST_TYPE_LOCATION);
    register_taxonomy_for_object_type('category','event-recurring');

    remove_filter('get_the_date',array('EM_Event_Post','the_date'),10,2);
}, 100);
