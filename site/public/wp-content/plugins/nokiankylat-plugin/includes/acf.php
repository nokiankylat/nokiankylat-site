<?php

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'nokiankylat_page_extra_settings',
		'name' => 'nokiankylat_page_extra_settings',
		'title' => 'Sivun lisäasetukset',
		'fields' => array (
			array (
				'key' => 'page_banner',
				'name' => 'page_banner',
				'label' => 'Yläosa',
				'type' => 'tab',
			),
			array (
				'key' => 'featured_posts',
				'name' => 'featured_posts',
				'label' => 'Yläosan nostolaatikot',
				'instructions' => 'Jos yhtään nostolaatikkoa ei ole määritelty, näytetään artikkelikuva.',
				'type' => 'repeater',
				'button_label' => 'Lisää laatikko',
				'sub_fields' => array (
					array (
						'key' => 'featured_posts_description',
						'label' => 'Teksti',
						'instructions' => 'Nostolaatikossa näytettävä teksti.',
						'name' => 'description',
						'type' => 'text',
					),
					array (
						'key' => 'featured_posts_target',
						'label' => 'Kohde',
						'instructions' => 'Valitse nostokohde tämän sivuston sisällöstä...',
						'name' => 'target',
						'type' => 'post_object',
						'return_format' => 'object',
						'ui' => 1,
					),
					array (
						'key' => 'featured_posts_target_url',
						'label' => 'Verkko-osoite',
						'instructions' => '...tai anna tässä mikä tahansa verkko-osoite, jos nostokohde ei ole tällä sivustolla.',
						'name' => 'target_url',
						'type' => 'text',
					),
					array (
						'key' => 'featured_posts_image',
						'label' => 'Kuva',
						'instructions' => 'Tämän sivuston kohteissa käytetään oletuksena artikkelikuvaa.',
						'name' => 'image',
						'type' => 'image',
						'return_format' => 'id',
					),
				),
			),
			array (
				'key' => 'left_sidebar',
				'name' => 'left_sidebar',
				'label' => 'Vasen sivupalsta',
				'type' => 'tab',
			),
			array (
				'key' => 'left_sidebar_displayed',
				'name' => 'left_sidebar_displayed',
				'message' => 'Näytä vasen sivupalsta',
				'type' => 'true_false',
				'default_value' => 1,
			),
			array (
				'key' => 'left_sidebar_content',
				'name' => 'left_sidebar_content',
				'label' => 'Vasemman sivupalstan sisältö',
				'instructions' => '
					Jos sisältöä ei ole, käytetään yläsivun sivupalkkia tai sivuston sivupalkkia.
					Sivuston sivupalkin asetukset ovat kohdassa
					<a href="' . site_url('/wp-admin/widgets.php') . '">Ulkoasu &gt; Vimpaimet.</a>
				',
				'type' => 'wysiwyg',
			),
			array (
				'key' => 'right_sidebar',
				'name' => 'right_sidebar',
				'label' => 'Oikea sivupalsta',
				'type' => 'tab',
			),
			array (
				'key' => 'right_sidebar_displayed',
				'name' => 'right_sidebar_displayed',
				'message' => 'Näytä oikea sivupalsta',
				'type' => 'true_false',
				'default_value' => 0,
			),
			array (
				'key' => 'right_sidebar_content',
				'name' => 'right_sidebar_content',
				'label' => 'Oikean sivupalstan sisältö',
				'instructions' => '
					Jos sisältöä ei ole, käytetään yläsivun sivupalkkia tai sivuston sivupalkkia.
					Sivuston sivupalkin asetukset ovat kohdassa Ulkoasu > Vimpaimet.
				',
				'type' => 'wysiwyg',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event',
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'event-recurring',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;