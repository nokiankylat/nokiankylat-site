<?php

/**
 * Include events in the default feed.
 * Props to http://www.wpbeginner.com/wp-tutorials/how-to-add-custom-post-types-to-your-main-wordpress-rss-feed/
 */
add_filter('request', function ($qv) {
	if (isset($qv['feed']) && !isset($qv['post_type'])) {
		$qv['post_type'] = array('post', 'event');
	}
	return $qv;
});

/**
 * Reduce feed cache transient lifetime.
 */
add_filter( 'wp_feed_cache_transient_lifetime' , function( $seconds ) {
	return 1 /* hour */ * 60 /* minutes */ * 60 /* seconds */;
});
