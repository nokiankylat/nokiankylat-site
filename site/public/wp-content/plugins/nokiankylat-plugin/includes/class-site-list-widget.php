<?php

/**
 * Widget that shows a list of sites in the network.
 */

namespace Wysiwyg\NokianKylat\Plugin\Site_List_Widget;

class Site_List_Widget extends \WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct( 'site_list', 'Kaikki sivustot' );
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $args['before_widget'];

		$title = '<a href="' . network_site_url() . '">Nokian kylät</a>';
//		$title = apply_filters( 'widget_title', $title );
		echo $before_title . $title . $after_title;

		global $wpdb;
		$oldblog = $wpdb->set_blog_id( 1 );
		wp_nav_menu( array( 'menu' => 'kylat' ) );
		$wpdb->set_blog_id( $oldblog ); ?>

		<div class="nokiankaupunki">
			<a href="http://www.nokiankaupunki.fi"><img
				src="<?php echo plugin_dir_url( __FILE__) . '../dist/images/nokia_logo_ln_transparent-200px.png'; ?>"
				alt="Nokian kaupunki" /></a>
		</div><!--  /.nokiankaupunki --><?php

  		echo $args['after_widget'];
	}
}

add_action( 'widgets_init', function() {
	register_widget( __NAMESPACE__ . '\Site_List_Widget' );
});