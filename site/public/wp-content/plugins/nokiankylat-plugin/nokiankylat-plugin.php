<?php
/*
Plugin Name: Nokian kylät -lisäosa
Description: Lisäosa Nokian kylien toiminnoille
Version:     1.0
Author:      Jarno Antikainen, Wysiwyg Oy
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: nokiankylat-plugin
*/
namespace Wysiwyg\NokianKylat\Plugin;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

require plugin_dir_path( __FILE__ ) . 'includes/acf.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-site-list-widget.php';
require plugin_dir_path( __FILE__ ) . 'includes/events-manager.php';
require plugin_dir_path( __FILE__ ) . 'includes/feeds.php';
require plugin_dir_path( __FILE__ ) . 'includes/shortcode-rss-feeds-mix.php';
require plugin_dir_path( __FILE__ ) . 'includes/tinymce.php';

function the_post_thumbnail_caption() {
	global $post;

 	$thumbnail_id    = get_post_thumbnail_id($post->ID);
 	$thumbnail_image = get_post($thumbnail_id);

	if ($thumbnail_image && $thumbnail_image->post_excerpt) {
		echo '<span class="the-post-thumbnail-caption">' . $thumbnail_image->post_excerpt . '</span>';
	}
}

add_filter( 'media_view_settings', function( $settings ) {
	$settings['galleryDefaults']['link'] = 'file';
	return $settings;
});
